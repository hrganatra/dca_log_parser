#!/usr/local/bin/perl -w
use POSIX;
use DBI;

#write locking mechanism. 

#Declaring Variables.
$HOST="cdtsorl306p.rxcorp.com";
$SID="PDCPADM1";
$PORT="1521";
$USER="DCA_DATA";
$PASS="DCA_DATA";
$PID=$$;

$FROM="/usr/local/production/containers/DCA-LOGS/in/"; 
$TO="/usr/local/production/containers/DCA-LOGS/out/";
$TO_NEW="/usr/local/production/containers/DCA-LOGS/out_new_format/";
$LOGDIR="/usr/local/production/log/";
#$INSITE="/data/dcpint/IN/INDCALOG01/out/";
$IESITE="/data/dcpint/IE/IEDCALOGS01/out/";
#$JPSITE="/data/dcpint/JP/JPDCALOG01/out/";
$LOGFILE="dca_logstash.log";

$SCP_HOST="cdtsdcpa21p";
$SCP_USER="prodpflow";
$SCP_DIR="/data/dca_log_parser/logstash-input/";
$SCP_NEW_DIR="/data/new_dca_format/logstash-input/";

open (LOG, ">>$LOGDIR$LOGFILE");

sub WRITELOG {
$STRING = shift; $FILENAME = shift;
$DATETIME = `date "+%Y-%m-%d %T"`;
chomp($DATETIME);
print LOG "$DATETIME $PID [$FILENAME] $STRING\n";
}

eval {$dbh = DBI->connect("dbi:Oracle:HOST=$HOST;SID=$SID;PORT=$PORT",$USER,$PASS, {RaiseError => 1});};
	if ($@) {
		WRITELOG("ERROR Unable to connect to Database, please ensure the connectivity and to run the script again","");
		exit 1;
}

sub SENDFILES {
	#system("chmod 777 $TO*");
	opendir (OUT1, $TO);
	@OUTLIST1 = readdir(OUT1);
	closedir OUT1;
	if ($#OUTLIST1 > 2) {
		system("scp -q $TO* $SCP_USER\@$SCP_HOST\:$SCP_DIR");
		#unlink glob "'$TO*'";
		system("rm -f $TO*");
	}
	sleep 5;
	opendir (OUT2, $TO_NEW);
        @OUTLIST2 = readdir(OUT2);
        closedir OUT2;
	if ($#OUTLIST2 > 2) {
	#system("chmod 777 $TO_NEW*");
		system("scp -q $TO_NEW* $SCP_USER\@$SCP_HOST\:$SCP_NEW_DIR");
		#unlink glob "'$TO_NEW*'";
		system("rm -f $TO_NEW*");
	}
}

opendir (FR, $FROM);
@DIRLIST = readdir(FR);
closedir FR;

foreach $DIR (@DIRLIST) {
next if ($DIR =~ m/^\./);
undef @FILELIST;
$CC=$DIR=~ /(..)/;
$CC=$1;
$DD=$DIR=~ /..([A-Za-z0-9]+).*/;
$DD = $1;
opendir (DIR, $FROM.$DIR);
@FILELIST = readdir(DIR);
close DIR;
	foreach $INPUTLOGFILE (@FILELIST) {
		next if ($INPUTLOGFILE =~ m/^\./);
		if ($INPUTLOGFILE =~ /debug/) {
			unlink $FROM.$DIR."/".$INPUTLOGFILE;
			next;
		}
		if (-z "$FROM$DIR/$INPUTLOGFILE")
		{
			WRITELOG("$INPUTLOGFILE is Empty",$INPUTLOGFILE);
			unlink $FROM.$DIR."/".$INPUTLOGFILE;
			next;
		}
		$OUTFILE=$INPUTLOGFILE.$PID;
		if ($INPUTLOGFILE =~ /IESOUAT/) {
			system("mv -f $FROM$DIR/$INPUTLOGFILE $IESITE");
        	        WRITELOG("Moving the UAT file to IEDCALOGS01",$INPUTLOGFILE);
			next;
		}
		#An old process to seperate out the New Format Logs.
		# $PRECHECK=0;
		# open (PC, $FROM.$DIR."/".$INPUTLOGFILE);
		# while (<PC>) {
			# if ($_ =~ /^\[/) {
				# if ($CC eq 'in') {
					# $PRECHECK=1;
					# system ("cp -f $FROM$DIR/$INPUTLOGFILE $INSITE");
					# WRITELOG("File $INPUTLOGFILE is in new format, hence file moved to $INSITE",$INPUTLOGFILE);
					# last;
				# }
				# elsif ($CC eq 'ie') {
					# $PRECHECK=1;
					# system("cp -f $FROM$DIR/$INPUTLOGFILE $IESITE");
					# WRITELOG("File $INPUTLOGFILE is in new format, hence file moved to $IESITE",$INPUTLOGFILE);
					# last;
				# }
				# elsif ($CC eq 'jp') {
					# $PRECHECK=1;
					# system ("cp -f $FROM$DIR/$INPUTLOGFILE $JPSITE");
					# WRITELOG("File $INPUTLOGFILE is in new format, hence file moved to $JPSITE",$INPUTLOGFILE);
					# last
				# }
			# }
		# }
		# close PC;

		# if ($PRECHECK == 1) {
			# unlink $FROM.$DIR."/".$INPUTLOGFILE;
			# next;
		# }

		open (FMAT, $FROM.$DIR."/".$INPUTLOGFILE);
		$FORMAT=<FMAT>;
		close FMAT;
		if ($FORMAT =~ /^\[/) {
			open (OUT, ">$TO_NEW$OUTFILE");
			system ("chmod 777 $TO_NEW$OUTFILE");
		}
		elsif ($FORMAT =~ /^(\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2},\d{3})/) {
			open (OUT, ">$TO$OUTFILE");
			system ("chmod 777 $TO$OUTFILE");
		}
		else {
			WRITELOG("File $INPUTLOGFILE Invalid",$INPUTLOGFILE);
			next;
		}

			
		if ($CC eq 'in') {
			$SITE_ID=$INPUTLOGFILE=~ /([A-Z0-9]+)/;
			$SITE_ID=$1;
			$SUPPLIER_ID=$INPUTLOGFILE=~ /^.*_dca-(.....).*/;
			$SUPPLIER_ID=$1;
			$CTRY_ISO_CD="IN";
			$COUNTRY="INDIA";
			if ($SUPPLIER_ID !~ /\d{5}/) {
				print "SUPPLIER ID for $INPUTLOGFILE is not Valid, please check the file $INPUTLOGFILE\n";
				WRITELOG ("ERROR SUPPLIER ID for $INPUTLOGFILE is not Valid, please check the file $INPUTLOGFILE",$INPUTLOGFILE);
				#Write code to move file to invalid folder.
				print "$INPUTLOGFILE skipped for processing and moved to invalid folder\n";
				WRITELOG("ERROR $INPUTLOGFILE skipped for processing and moved to invalid folder",$INPUTLOGFILE);
				next;
			}
		}
		if ($CC eq 'ie') {
			$SITE_ID = $INPUTLOGFILE =~ /([A-Z0-9]+)\_.*/;
			$SITE_ID = $1;
			$CTRY_ISO_CD="IE";
			$COUNTRY="IRELAND";
			if ($SITE_ID =~ /(MCL)|(LLY)/) {
				$SUPPLIER_ID = $INPUTLOGFILE =~ /[A-Za-z0-9\_\-]\.([A-Za-z\_]+)\.*/;
				$SUPPLIER_ID = $1."usr";
			}
			else {
				$SUPPLIER_ID = $INPUTLOGFILE =~ /[A-Za-z0-9\_\-]\.([A-Za-z0-9\_]+)\.*/;
				$SUPPLIER_ID = $1;
			}
		}
		if ($CC eq 'jp') {
			if ($DD eq 'whs') {
				$SUPPLIER_ID = $INPUTLOGFILE =~ /dca\-audit\-(\d+)\.[A-Z0-9a-z\_\.]+/;
				$SUPPLIER_ID = "WHS".$1;
				$CTRY_ISO_CD="JP";
				$SITE_ID=$SUPPLIER_ID;
				$COUNTRY="JAPAN";
			}
			elsif ($DD eq 'rx') {
				if ($INPUTLOGFILE =~ /(L7025|L7076|L7027|L7026)/) {
				$SITE_ID = $INPUTLOGFILE =~ /([A-Z0-9]+)\_.*/;
	                        $SITE_ID = $1;
				$SUPPLIER_ID = $INPUTLOGFILE =~ /[A-Za-z0-9\_\-]\.([A-Za-z0-9\_]+)\.*/;
                                $SUPPLIER_ID = $1;
                                $CTRY_ISO_CD="JP";
                                $COUNTRY="JAPAN";
				}
				else {
				unlink $FROM.$DIR."/".$INPUTLOGFILE;
				next;
				}
			}
		}
		$sql="select distinct f.USR_ID as INSTALLATION_USR_ID, c.ORG_ID as ORG_IDENTIFIER, c.ORG_NM,d.TM_ZONE_CD as TIMEZONE, b.LOC_ADDR_LINE_1_TXT, b.CITY_NM,d.INSTALLATION_KEY from DC_ORG_GEO_LOC a right join DC_GEO_LOC b on a.GEO_LOC_ID=b.GEO_LOC_ID right join DC_ORG c on a.ORG_ID=c.ORG_ID right join DC_DCA_INSTALLATION d on d.ORG_GEO_LOC_ID=a.ORG_GEO_LOC_ID right join DC_PERS_AUTH_DETLS_D e on e.ORG_GEO_LOC_ID=a.ORG_GEO_LOC_ID right join DC_PERS_AUTH_DETLS f on f.PERS_ID=e.PERS_ID where f.USR_ID=\'$SUPPLIER_ID\' and d.INSTALLATION_KEY is not null";

		$sth = $dbh->prepare("$sql");
		$sth->execute();
	
		@data = $sth->fetchrow_array();

		if ($#data eq '6') {
			$SUPPLIER_ID=$data[0];
			$ORG_ID=$data[1];
			$ORG_NM=$data[2];
			$TIMEZONE=$data[3];
			if (! defined $data[4]) { $ADD_LINE_1=' '; }
			else { $ADD_LINE_1=$data[4]; }
			if (! defined $data[5]) { $CITY=' '; }
			else { $CITY=$data[5]; }
			$INSTALLATION_KEY=$data[6];
			$ORG_STATUS=' ';
		}
		else {
			$ORG_ID=' ';
			$ORG_NM=' ';
			$TIMEZONE=' ';
			$ADD_LINE_1=' ';
			$CITY=' ';
			$INSTALLATION_KEY=' ';
			$ORG_STATUS=' ';
		}
	
		if ($ADD_LINE_1 eq '') {
			$ADD_LINE_1 =~ s//NULL/;
		}

		open (IN, $FROM.$DIR."/".$INPUTLOGFILE);
		#open (OUT, ">$TO$OUTFILE");
		#open (OUT_NEW, ">$TO_NEW$OUTFILE");
		while (<IN>) {
			$LINE = "\'".$SUPPLIER_ID."\' \'".$SITE_ID."\' \'".$ORG_ID."\' \'".$ORG_NM."\' \'".$ORG_STATUS."\' \'".$TIMEZONE."\' \'".$ADD_LINE_1."\' \'".$CITY."\' \'".$COUNTRY."\' \'".$CTRY_ISO_CD."\' \'".$INSTALLATION_KEY."\' \'".$INPUTLOGFILE."\' ".$_;
			$LINE =~ s/\x04//g;
			$LINE =~ s/\xa0/ /g;
			$LINE =~ s/\xA0/ /g;
			$LINE =~ s/\x10//g;
			if ($_ =~ /^(\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2},\d{3})|(^\[\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2},\d{3}\])/) {
				print OUT $LINE;
			}
		}
		close IN;
		unlink $FROM.$DIR."/".$INPUTLOGFILE;
		#system("chmod 777 $TO$OUTFILE");
		close OUT;
		#close OUT_NEW;
		#WRITELOG ("$INPUTLOGFILE Processed Successfully",$INPUTLOGFILE);
	}
}
WRITELOG("Sending Files to Logstash Central","");
SENDFILES();
close LOG;
print "Process Completed\n";
