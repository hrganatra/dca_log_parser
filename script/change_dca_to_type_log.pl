#use warnings;
#use WWW::Mechanize;	
use POSIX;
#use Encode::Byte;
#use File::ShareDir::PAR;

#To Check if ES is running or not.
print "Checking if ES is running or not\n";
# $mech  = WWW::Mechanize->new();
# $url  = "http://localhost:9200/";

$date = strftime "%d%m%Y", localtime;

# eval { $mech->get($url); };
	# if ($@) { 
		# print "ES is not running, please start ES and run script again\n"; 
		# exit;
	# }

# Write code to see if logstash is running or not.
	
#Declaring Variables.
$LOGINPUTDIR="../logsinput/";
$LOGOUTPUTDIR="../logsoutput/";
$INFILE="IEMCLDCA01_dca-audit.mcl_dcainstllaer.2015-11-27_13.log"; #Later change to read dir and parse all files.
$OUTFILE=$INFILE.$date;
$LOGFILE="script_log.log";
$OUTINFOFILE="type_log.log";

#Get info from Filename

$CC=$INFILE=~ /(..)/;
$CC=$1;
$SITE_ID=$INFILE=~ /([A-Z0-9]+)/;
$SITE_ID=$1;
$ORG_NM=$INFILE=~ /..(...)/;
$ORG_NM=$1;

open (IN, "$LOGINPUTDIR$INFILE");
open (OUTINFO, ">$LOGOUTPUTDIR$OUTINFOFILE");
open (LOG, ">>$LOGOUTPUTDIR$LOGFILE");


# Filename: IEMCLDCA01_dca-audit.mcl_dcainstllaer.2015-11-27_13.log
# Country Code = IE
# Site ID = IEMCLDCA01
# Org Name = MCL
# Created Date (from file properties)
# File Size:

#To Count Number of Document ID's in a particular index in ES.
# sub COUNT {
	# $index = shift;
	# $mech  = WWW::Mechanize->new();
	# $url  = "http://localhost:9200/".$index."/_count/";
	# print LOG "Counting Document ID for ".$url."\n";

	# eval { $mech->get($url); };
		# if ($@) { 
			# $count = 0;
			# print LOG "Index is not created, setting counter to 0\n"; 
			# return $count;
		# }
		# else {
			# $text = $mech->content;

			# if ($text =~ /([\d]+)/) {
				# $count = $1;
				# print LOG "Document ID Count is ".$count."\n";
				# #print $1."\n";
				# return $count;
			# }
	# }
# }

# #Counting number of document id's
# $CNT = COUNT("dcalogs");

print LOG "Processing File $INFILE \n";
$i=0;
while (<IN>) {
	$CNT++;
	$_ =~ s/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2},\d{3}/\{timeStamp\}/;
#	$_ =~ s/(\d{7}|[A-Za-z0-9]+)-(\d{4}|[A-Za-z0-9]+)-(\d{4}|[A-Za-z0-9]+)-(\d{4}|[A-Za-z0-9]+)-(\d{12}|[A-Za-z0-9]+)/\{pid\}/;
	$_ =~ s/INFO/\{logType\}/;
	$_ =~ s/WARN/\{logType\}/;
	$_ =~ s/ERROR/\{logType\}/;
	$_ =~ s/\[[A-Za-z]+\-[A-Za-z]+\@/\[\{pluginName\}\@/;
	$_ =~ s/\[[a-z]+\@/\[\{pluginName\}\@/;
	$_ =~ s/\d{1}\.\d{1}\.\d{1,2}/\{pluginVersion\}/;
	$_ =~ s/prescriptions\//\{processName\}\//;
	$_ =~ s/logs-process\//\{processName\}\//;
	$_ =~ s/c\.i\.d\.service\.filesaver\.FileSaverService/\{component\}/;
	$_ =~ s/c\.i\.d\.d\.service\.DataValidatorService/\{component\}/;
	$_ =~ s/c\.i\.d\.s\.e\.impl\.EncryptionActivityImpl/\{component\}/;
	$_ =~ s/c\.i\.d\.s\.f\.ftp\.SftpUploadService/\{component\}/;
	$_ =~ s/c\.i\.d\.s\.f\.LocalSourceFileRemoverService/\{component\}/;
	$_ =~ s/c\.i\.d\.s\.p\.impl\.DefaultProcessService/\{component\}/;
	$_ =~ s/c\.i\.d\.s\.filepicker\.FilePickerService/\{component\}/;
	$_ =~ s/c\.i\.d\.c\.u\.impl\.CmConfigurationUpdater/\{component\}/;
	$_ =~ s/c\.i\.d\.s\.updater\.impl\.ObrBundlesUpdater/\{component\}/;
	$_ =~ s/c\.i\.d\.s\.h\.impl\.RestSendHeartbeatService/\{component\}/;
	$_ =~ s/c\.i\.d\.s\.audit\.service\.AuditServiceImpl/\{component\}/;
	$_ =~ s/c\.i\.d\.filesplit\.service\.FileSplitService/\{component\}/;
	$_ =~ s/(\d{8}|([A-Za-z0-9]+))\-(\d{4}|[A-Za-z0-9]+)\-(\d{4}|[A-Za-z0-9]+)\-(\d{4}|[A-Za-z0-9]+)\-(\d{12}|[A-Za-z0-9]+)/\{pid\}/;
	$_ =~ s/(B|D|P|S|T|K)\_(MCL|TCH)\_\d{5}\_\d{8}\.\d{3}\.index/\{Filename\}/g;
	$_ =~ s/(B|D|P|S|T|K)\_(MCL|TCH)\_\d{5}\_\d{8}\_\d{3}\.\d{3}.mapping/\{Filename\}/g;
	$_ =~ s/(B|D|P|S|T|K)\_(MCL|TCH)\_\d{5}\_\d{8}\.\d{3}.enc/\{Filename\}/g;
	$_ =~ s/(B|D|P|S|T|K)\_(MCL|TCH)\_\d{5}\_\d{8}\.\d{3}/\{Filename\}/g;
	$_ =~ s/(B|D|P|S|T|K)\_(MCL|TCH)\_\d{5}\_\d{8}\_\d{3}\.\d{3}/\{Filename\}/g;
	$_ =~ s/record \= \d+/record \=\{record\}/;
	$_ =~ s/lineNumber \= \d+/lineNumber \= \{lineNumber\}\}/;
	$_ =~ s/valuePosition = \d+/valuePosition \= \{valuePosition\}\}/;
	
	$_ =~ s/\x04//g;
	$_ =~ s/\xa0/ /g;
	$_ =~ s/\x10//g;
	
	print OUTINFO "{id} {cc} {siteId} {orgName}"." ".$_;
}

#Move file to processed folder once the input file is processed.

print LOG "Process Completed\n";

close IN;
close OUTINFO;
close OUTWARN;
close OUTERR;
close OUTDEBUG;
close LOG;

#The only problem with this is that the file info.log, warn.log, etc will increase a lot. One way to solve it is to put dates in file and parse all info.yyyymmdd.log type of files.