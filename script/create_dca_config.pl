use warnings;

open (C, ">", "../config/dca_test.config");

print C "input {\n";
print C "\tfile {\n";
print C "\t\tpath => \"C:/Elasticsearch/dca_log_parser/logsoutput/IEMCLDCA01*.log\"\n";
print C "\t\tstart_position => beginning \n";
print C "\t}\n";
print C "}\n";
print C "\n";
print C "filter {\n";
print C "\tgrok \n";


open (R, "match.txt");

while (<R>) {
chomp;
print C "\tif \"_grokparsefailure\" in [tags] {\n";
print C "\t  grok {\n";
print C "\t\tpatterns_dir => \"C:/Elasticsearch/dca_log_parser/patterns\"\n";
print C "\t\tmatch => { \"message\" => \"$_\" }\n";
print C "\t\tremove_tag => [\"_grokparsefailure\"]\n";
print C "\t  }\n";
print C "\t}\n";
}

print C "\tmutate {\n";
print C "\t\t# remove duplicate fields\n";
print C "\t\t# this leaves timestamp from message and source_path for source\n";
print C "\t\tremove_field => \[\"\@version\",\"message\",\"path\",\"host\",\"tags\"\]\n";
print C "\t}\n";
print C "}\n";
print C "\n";
print C "output {\n";
print C "\telasticsearch {\n";
print C "\tindex => dcalogs\n";
print C "\tdocument_id => \"%{ID}\"\n";
print C "\ttemplate => \"C:/Users/hrganatra/Desktop/Securnet/Work/dca_log_parser/config/template.json\"\n";
print C "\t}\n";
print C "\tfile {\n";
print C "\t\tpath => \"C:/Elasticsearch/dca_log_parser/logsoutput/output.log\"\n";
print C "\t\tcodec => json_lines  {}\n";
print C "\t}\n";
print C "}\n";

close C;
close R;